package com.ruoyi.web.controller.tool;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginBody;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.web.service.SysLoginService;
import com.ruoyi.system.domain.ProductPrice;
import com.ruoyi.system.service.IProductPriceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiJsonModelBuilder;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api("HTTP接口管理")
@RestController
@RequestMapping("/http-api")
public class HttpApiController  extends BaseController {

    @Autowired
    private IProductPriceService productPriceService;

    @Autowired
    private SysLoginService loginService;


    @ApiOperation("用户名密码登录")
    @PostMapping("/login")
    @ApiOperation(value = "修改地址 - ApiJsonModel")
    @ApiJsonModel({
            @ApiModelProperty(name = "id", value = "收件人电话"),
            @ApiModelProperty(name = "telNumber", value = "收货地址id")
    })
    public AjaxResult login(@RequestBody String data) {

        JSONObject dataObj = JSONObject.parseObject(data);
        if (dataObj == null)
        {
            logger.error("API login fail,data is null");
            return AjaxResult.error("无效参数");
        }

        String userName = dataObj.getString("userName");
        String password = dataObj.getString("password");
        String uuid = dataObj.getString("uuid");
        String validateCode = dataObj.getString("validateCode");
        boolean smsLogin = dataObj.getBooleanValue("smsLogin");
        boolean registNoExist = dataObj.getBooleanValue("registNoExist");
        if (StringUtils.isEmpty(userName))
        {
            logger.error("user name is null");
            return AjaxResult.error("no userName");
        }
        AjaxResult ajax = AjaxResult.success();
        if (smsLogin == true)
        {
            //sms login
            if (StringUtils.isEmpty(validateCode))
            {
                logger.error("validateCode is null");
                return AjaxResult.error("no validateCode");
            }
            if (StringUtils.isEmpty(uuid))
            {
                logger.error("uuid is null");
                return AjaxResult.error("no uuid");
            }
            try {
                String token = loginService.smsLogin(userName, validateCode, uuid, registNoExist);
                ajax.put(Constants.TOKEN, token);
                return ajax;
            } catch (Exception e) {
                String msg;
                if (StringUtils.isNotEmpty(e.getMessage())) {
                    msg = e.getMessage();
                }
                else {
                    msg = "登录失败";
                }
                return error(msg);
            }
        }
        else {
            if (StringUtils.isEmpty(password))
            {
                logger.error("password is null");
                return AjaxResult.error("no password");
            }
            try {
                String token = loginService.login(userName, password, validateCode, uuid);
                ajax.put(Constants.TOKEN, token);
                return ajax;
            } catch (Exception e) {
                String msg;
                if (StringUtils.isNotEmpty(e.getMessage())) {
                    msg = e.getMessage();
                }
                else {
                    msg = "登录失败";
                }
                return error(msg);
            }
        }
    }

    @ApiOperation("获取产品价格表")
    @ApiImplicitParam(name = "productId", value = "产品ID", required = true, dataType = "int", paramType = "path", dataTypeClass = Long.class)
    @GetMapping("getPriceList/{productId}")
    public AjaxResult getPriceList(@PathVariable Integer productId)
    {
        ProductPrice productPrice = new ProductPrice();
        productPrice.setProductId(productId.longValue());
        List<ProductPrice> list = productPriceService.selectProductPriceList(productPrice);
        JSONArray jsonArray = new JSONArray();
        for (ProductPrice price : list)
        {
            jsonArray.add(price.toJsonObject());
        }
        AjaxResult ajax = AjaxResult.success();
        ajax.put("data", jsonArray);
        return ajax;
    }
}
