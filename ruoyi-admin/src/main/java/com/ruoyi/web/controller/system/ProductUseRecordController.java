package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ProductUseRecord;
import com.ruoyi.system.service.IProductUseRecordService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 产品使用记录Controller
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@RestController
@RequestMapping("/product/userecord")
public class ProductUseRecordController extends BaseController
{
    @Autowired
    private IProductUseRecordService productUseRecordService;

    /**
     * 查询产品使用记录列表
     */
    @PreAuthorize("@ss.hasPermi('product:userecord:list')")
    @GetMapping("/list")
    public TableDataInfo list(ProductUseRecord productUseRecord)
    {
        startPage();
        List<ProductUseRecord> list = productUseRecordService.selectProductUseRecordList(productUseRecord);
        return getDataTable(list);
    }

    /**
     * 导出产品使用记录列表
     */
    @PreAuthorize("@ss.hasPermi('product:userecord:export')")
    @Log(title = "产品使用记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ProductUseRecord productUseRecord)
    {
        List<ProductUseRecord> list = productUseRecordService.selectProductUseRecordList(productUseRecord);
        ExcelUtil<ProductUseRecord> util = new ExcelUtil<ProductUseRecord>(ProductUseRecord.class);
        util.exportExcel(response, list, "产品使用记录数据");
    }

    /**
     * 获取产品使用记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('product:userecord:query')")
    @GetMapping(value = "/{useId}")
    public AjaxResult getInfo(@PathVariable("useId") Long useId)
    {
        return AjaxResult.success(productUseRecordService.selectProductUseRecordByUseId(useId));
    }

    /**
     * 新增产品使用记录
     */
    @PreAuthorize("@ss.hasPermi('product:userecord:add')")
    @Log(title = "产品使用记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ProductUseRecord productUseRecord)
    {
        return toAjax(productUseRecordService.insertProductUseRecord(productUseRecord));
    }

    /**
     * 修改产品使用记录
     */
    @PreAuthorize("@ss.hasPermi('product:userecord:edit')")
    @Log(title = "产品使用记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ProductUseRecord productUseRecord)
    {
        return toAjax(productUseRecordService.updateProductUseRecord(productUseRecord));
    }

    /**
     * 删除产品使用记录
     */
    @PreAuthorize("@ss.hasPermi('product:userecord:remove')")
    @Log(title = "产品使用记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{useIds}")
    public AjaxResult remove(@PathVariable Long[] useIds)
    {
        return toAjax(productUseRecordService.deleteProductUseRecordByUseIds(useIds));
    }
}
