package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.AuthTimeType;
import com.ruoyi.system.domain.ProductType;
import com.ruoyi.system.service.IAuthTimeTypeService;
import com.ruoyi.system.service.IProductTypeService;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ProductPrice;
import com.ruoyi.system.service.IProductPriceService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 价格Controller
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@RestController
@RequestMapping("/product/price")
public class ProductPriceController extends BaseController
{
    @Autowired
    private IProductPriceService productPriceService;
    @Autowired
    private IProductTypeService productTypeService;
    @Autowired
    private IAuthTimeTypeService authTimeTypeService;

    /**
     * 查询价格列表
     */
    @PreAuthorize("@ss.hasPermi('product:price:list')")
    @GetMapping("/list")
    public TableDataInfo list(ProductPrice productPrice)
    {
        startPage();
        List<ProductPrice> list = productPriceService.selectProductPriceList(productPrice);
        return getDataTable(list);
    }

    @ApiOperation("获取价格表")
    @GetMapping("getPriceList")
    public AjaxResult getPriceList()
    {
        ProductPrice productPrice = new ProductPrice();
        List<ProductPrice> list = productPriceService.selectProductPriceList(productPrice);
        String str;
        for (int i=0; i< list.size(); i++)
        {
            ProductPrice p = list.get(i);
            str = p.toString();

        }
        AjaxResult ajax = AjaxResult.success();
        ajax.put("priceList", list);
        return ajax;
    }

    /**
     * 导出价格列表
     */
    @PreAuthorize("@ss.hasPermi('product:price:export')")
    @Log(title = "价格", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ProductPrice productPrice)
    {
        List<ProductPrice> list = productPriceService.selectProductPriceList(productPrice);
        ExcelUtil<ProductPrice> util = new ExcelUtil<ProductPrice>(ProductPrice.class);
        util.exportExcel(response, list, "价格数据");
    }

    /**
     * 获取价格详细信息
     */
    @PreAuthorize("@ss.hasPermi('product:price:query')")
    @GetMapping(value = { "/", "/{priceId}" })
    public AjaxResult getInfo(@PathVariable(value = "priceId", required = false) Long priceId)
    {
        AjaxResult ajax = AjaxResult.success();
        List<ProductType> products = productTypeService.selectProductTypeAll();
        ajax.put("productTypes", products);
        List<AuthTimeType> authTimes = authTimeTypeService.selectAuthTimeTypeAll();
        ajax.put("authTimeTypes", authTimes);

        if (StringUtils.isNotNull(priceId))
        {
            ProductPrice price = productPriceService.selectProductPriceByPriceId(priceId);
            ajax.put(AjaxResult.DATA_TAG, price);
        }
        return ajax;

//        return AjaxResult.success(productPriceService.selectProductPriceByPriceId(priceId));
    }

    /**
     * 新增价格
     */
    @PreAuthorize("@ss.hasPermi('product:price:add')")
    @Log(title = "价格", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ProductPrice productPrice)
    {
        return toAjax(productPriceService.insertProductPrice(productPrice));
    }

    /**
     * 修改价格
     */
    @PreAuthorize("@ss.hasPermi('product:price:edit')")
    @Log(title = "价格", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ProductPrice productPrice)
    {
        return toAjax(productPriceService.updateProductPrice(productPrice));
    }

    /**
     * 删除价格
     */
    @PreAuthorize("@ss.hasPermi('product:price:remove')")
    @Log(title = "价格", businessType = BusinessType.DELETE)
	@DeleteMapping("/{priceIds}")
    public AjaxResult remove(@PathVariable Long[] priceIds)
    {
        return toAjax(productPriceService.deleteProductPriceByPriceIds(priceIds));
    }
}
