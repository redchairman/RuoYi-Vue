package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ProductType;
import com.ruoyi.system.service.IProductTypeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 产品Controller
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@RestController
@RequestMapping("/product/type")
public class ProductTypeController extends BaseController
{
    @Autowired
    private IProductTypeService productTypeService;

    /**
     * 查询产品列表
     */
    @PreAuthorize("@ss.hasPermi('product:type:list')")
    @GetMapping("/list")
    public TableDataInfo list(ProductType productType)
    {
        startPage();
        List<ProductType> list = productTypeService.selectProductTypeList(productType);
        return getDataTable(list);
    }

    /**
     * 导出产品列表
     */
    @PreAuthorize("@ss.hasPermi('product:type:export')")
    @Log(title = "产品", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ProductType productType)
    {
        List<ProductType> list = productTypeService.selectProductTypeList(productType);
        ExcelUtil<ProductType> util = new ExcelUtil<ProductType>(ProductType.class);
        util.exportExcel(response, list, "产品数据");
    }

    /**
     * 获取产品详细信息
     */
    @PreAuthorize("@ss.hasPermi('product:type:query')")
    @GetMapping(value = "/{productId}")
    public AjaxResult getInfo(@PathVariable("productId") Long productId)
    {
        return AjaxResult.success(productTypeService.selectProductTypeByProductId(productId));
    }

    /**
     * 新增产品
     */
    @PreAuthorize("@ss.hasPermi('product:type:add')")
    @Log(title = "产品", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ProductType productType)
    {
        return toAjax(productTypeService.insertProductType(productType));
    }

    /**
     * 修改产品
     */
    @PreAuthorize("@ss.hasPermi('product:type:edit')")
    @Log(title = "产品", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ProductType productType)
    {
        return toAjax(productTypeService.updateProductType(productType));
    }

    /**
     * 删除产品
     */
    @PreAuthorize("@ss.hasPermi('product:type:remove')")
    @Log(title = "产品", businessType = BusinessType.DELETE)
	@DeleteMapping("/{productIds}")
    public AjaxResult remove(@PathVariable Long[] productIds)
    {
        return toAjax(productTypeService.deleteProductTypeByProductIds(productIds));
    }
}
