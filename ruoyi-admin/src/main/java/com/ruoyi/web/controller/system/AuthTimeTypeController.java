package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.AuthTimeType;
import com.ruoyi.system.service.IAuthTimeTypeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 授权时间类型Controller
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@RestController
@RequestMapping("/auth/timeType")
public class AuthTimeTypeController extends BaseController
{
    @Autowired
    private IAuthTimeTypeService authTimeTypeService;

    /**
     * 查询授权时间类型列表
     */
    @PreAuthorize("@ss.hasPermi('auth:timeType:list')")
    @GetMapping("/list")
    public TableDataInfo list(AuthTimeType authTimeType)
    {
        startPage();
        List<AuthTimeType> list = authTimeTypeService.selectAuthTimeTypeList(authTimeType);
        return getDataTable(list);
    }

    /**
     * 导出授权时间类型列表
     */
    @PreAuthorize("@ss.hasPermi('auth:timeType:export')")
    @Log(title = "授权时间类型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AuthTimeType authTimeType)
    {
        List<AuthTimeType> list = authTimeTypeService.selectAuthTimeTypeList(authTimeType);
        ExcelUtil<AuthTimeType> util = new ExcelUtil<AuthTimeType>(AuthTimeType.class);
        util.exportExcel(response, list, "授权时间类型数据");
    }

    /**
     * 获取授权时间类型详细信息
     */
    @PreAuthorize("@ss.hasPermi('auth:timeType:query')")
    @GetMapping(value = "/{timeId}")
    public AjaxResult getInfo(@PathVariable("timeId") Long timeId)
    {
        return AjaxResult.success(authTimeTypeService.selectAuthTimeTypeByTimeId(timeId));
    }

    /**
     * 新增授权时间类型
     */
    @PreAuthorize("@ss.hasPermi('auth:timeType:add')")
    @Log(title = "授权时间类型", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AuthTimeType authTimeType)
    {
        return toAjax(authTimeTypeService.insertAuthTimeType(authTimeType));
    }

    /**
     * 修改授权时间类型
     */
    @PreAuthorize("@ss.hasPermi('auth:timeType:edit')")
    @Log(title = "授权时间类型", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AuthTimeType authTimeType)
    {
        return toAjax(authTimeTypeService.updateAuthTimeType(authTimeType));
    }

    /**
     * 删除授权时间类型
     */
    @PreAuthorize("@ss.hasPermi('auth:timeType:remove')")
    @Log(title = "授权时间类型", businessType = BusinessType.DELETE)
	@DeleteMapping("/{timeIds}")
    public AjaxResult remove(@PathVariable Long[] timeIds)
    {
        return toAjax(authTimeTypeService.deleteAuthTimeTypeByTimeIds(timeIds));
    }
}
