package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.SecurityUtils;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ProductOrder;
import com.ruoyi.system.service.IProductOrderService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 订单管理Controller
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@RestController
@RequestMapping("/product/order")
public class ProductOrderController extends BaseController
{
    @Autowired
    private IProductOrderService productOrderService;

    /**
     * 查询订单管理列表
     */
    @PreAuthorize("@ss.hasPermi('product:order:list')")
    @GetMapping("/list")
    public TableDataInfo list(ProductOrder productOrder)
    {
        startPage();
        List<ProductOrder> list = productOrderService.selectProductOrderList(productOrder);
        return getDataTable(list);
    }

    /**
     * 导出订单管理列表
     */
    @PreAuthorize("@ss.hasPermi('product:order:export')")
    @Log(title = "订单管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ProductOrder productOrder)
    {
        List<ProductOrder> list = productOrderService.selectProductOrderList(productOrder);
        ExcelUtil<ProductOrder> util = new ExcelUtil<ProductOrder>(ProductOrder.class);
        util.exportExcel(response, list, "订单管理数据");
    }

    /**
     * 获取订单管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('product:order:query')")
    @GetMapping(value = "/{orderId}")
    public AjaxResult getInfo(@PathVariable("orderId") Long orderId)
    {
        return AjaxResult.success(productOrderService.selectProductOrderByOrderId(orderId));
    }

    /**
     * 新增订单管理
     */
    @PreAuthorize("@ss.hasPermi('product:order:add')")
    @Log(title = "订单管理", businessType = BusinessType.INSERT)
    @ApiOperation("新建订单")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "productCode", value = "商品标识码", required = true, dataType = "String", dataTypeClass = String.class),
        @ApiImplicitParam(name = "priceId", value = "价格套餐ID", required = true, dataType = "Long", dataTypeClass = Long.class)
    })
    @PostMapping("/add")
    public AjaxResult add(@RequestBody ProductOrder productOrder)
    {
        SysUser user = SecurityUtils.getLoginUser().getUser();
        productOrder.setUserId(user.getUserId());
        AjaxResult ajax = toAjax(productOrderService.insertProductOrder(productOrder));
        ajax.put("qrcodeUrl", productOrder.getQrCodeUrl());
        //ajax.put("qrcodeTimeout", productOrder.getQrCodeUrl());
        return ajax;
    }

    /**
     * 修改订单管理
     */
    @PreAuthorize("@ss.hasPermi('product:order:edit')")
    @Log(title = "订单管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ProductOrder productOrder)
    {
        return toAjax(productOrderService.updateProductOrder(productOrder));
    }

    /**
     * 删除订单管理
     */
    @PreAuthorize("@ss.hasPermi('product:order:remove')")
    @Log(title = "订单管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{orderIds}")
    public AjaxResult remove(@PathVariable Long[] orderIds)
    {
        return toAjax(productOrderService.deleteProductOrderByOrderIds(orderIds));
    }
}
