package com.ruoyi.system.service.impl;

import java.util.Date;
import java.util.List;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import com.ruoyi.common.constant.OrderConstants;
import com.ruoyi.system.service.IWxPayV3Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ProductOrderMapper;
import com.ruoyi.system.domain.ProductOrder;
import com.ruoyi.system.service.IProductOrderService;

/**
 * 订单管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@Service
public class ProductOrderServiceImpl implements IProductOrderService 
{
    @Autowired
    private ProductOrderMapper productOrderMapper;

    @Autowired
    private IWxPayV3Service wxPayV3Service;

    /**
     * 查询订单管理
     * 
     * @param orderId 订单管理主键
     * @return 订单管理
     */
    @Override
    public ProductOrder selectProductOrderByOrderId(Long orderId)
    {
        return productOrderMapper.selectProductOrderByOrderId(orderId);
    }

    /**
     * 查询订单管理列表
     * 
     * @param productOrder 订单管理
     * @return 订单管理
     */
    @Override
    public List<ProductOrder> selectProductOrderList(ProductOrder productOrder)
    {
        return productOrderMapper.selectProductOrderList(productOrder);
    }

    /**
     * 新增订单管理
     * 
     * @param productOrder 订单管理
     * @return 结果
     */
    @Override
    public int insertProductOrder(ProductOrder productOrder)
    {
        String orderNumber = genOrderNumber();
        productOrder.setOrderNumber(orderNumber);

        String codeUrl = wxPayV3Service.nativePay(productOrder);
        productOrder.setQrCodeUrl(codeUrl);
        productOrder.setState(OrderConstants.ORDER_STATUS_WAIT_PAY);
        return productOrderMapper.insertProductOrder(productOrder);
    }

    private String genOrderNumber()
    {
        String format = DateUtil.format(new Date(),"yyMMddHHmmss");
        String rand = RandomUtil.randomNumbers(5);
        String number = format + rand;
        return number;
    }


    /**
     * 修改订单管理
     * 
     * @param productOrder 订单管理
     * @return 结果
     */
    @Override
    public int updateProductOrder(ProductOrder productOrder)
    {
        return productOrderMapper.updateProductOrder(productOrder);
    }

    /**
     * 批量删除订单管理
     * 
     * @param orderIds 需要删除的订单管理主键
     * @return 结果
     */
    @Override
    public int deleteProductOrderByOrderIds(Long[] orderIds)
    {
        return productOrderMapper.deleteProductOrderByOrderIds(orderIds);
    }

    /**
     * 删除订单管理信息
     * 
     * @param orderId 订单管理主键
     * @return 结果
     */
    @Override
    public int deleteProductOrderByOrderId(Long orderId)
    {
        return productOrderMapper.deleteProductOrderByOrderId(orderId);
    }
}
