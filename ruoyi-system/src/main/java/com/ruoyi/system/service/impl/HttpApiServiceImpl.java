package com.ruoyi.system.service.impl;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.domain.ProductPrice;
import com.ruoyi.system.service.IHttpApiService;
import com.ruoyi.system.service.IProductPriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class HttpApiServiceImpl implements IHttpApiService
{
    @Autowired
    private IProductPriceService productPriceService;

//    public AjaxResult login(String data) {
//
//        JSONObject dataObj = JSONObject.parseObject(data);
//        if (dataObj == null) {
//            logger.error("API login fail,data is null");
//            return AjaxResult.error("无效参数");
//        }
//
//        String userName = dataObj.getString("userName");
//        String password = dataObj.getString("password");
//        String mobile = dataObj.getString("mobile");
//        String validateCode = dataObj.getString("validateCode");
//        boolean registNoExist = dataObj.getBoolean("registNoExist");
//
//        if (smsCode != null && smsCode.isEmpty() == false)
//        {
//            //sms login
//        }
//        else
//        {
//
//        }
//
//    }

    public AjaxResult getPriceList()
    {
        ProductPrice productPrice = new ProductPrice();
        List<ProductPrice> list = productPriceService.selectProductPriceList(productPrice);
        JSONArray jsonArray = new JSONArray();
        for (ProductPrice price : list)
        {
            jsonArray.add(price.toJsonObject());
        }
        AjaxResult ajax = AjaxResult.success();
        ajax.put("data", jsonArray);
        return ajax;
    }


}
