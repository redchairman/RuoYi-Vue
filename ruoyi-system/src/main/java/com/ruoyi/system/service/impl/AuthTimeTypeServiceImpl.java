package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.domain.ProductType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.AuthTimeTypeMapper;
import com.ruoyi.system.domain.AuthTimeType;
import com.ruoyi.system.service.IAuthTimeTypeService;

/**
 * 授权时间类型Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@Service
public class AuthTimeTypeServiceImpl implements IAuthTimeTypeService 
{
    @Autowired
    private AuthTimeTypeMapper authTimeTypeMapper;

    /**
     * 查询授权时间类型
     * 
     * @param timeId 授权时间类型主键
     * @return 授权时间类型
     */
    @Override
    public AuthTimeType selectAuthTimeTypeByTimeId(Long timeId)
    {
        return authTimeTypeMapper.selectAuthTimeTypeByTimeId(timeId);
    }

    /**
     * 查询授权时间类型列表
     * 
     * @param authTimeType 授权时间类型
     * @return 授权时间类型
     */
    @Override
    public List<AuthTimeType> selectAuthTimeTypeList(AuthTimeType authTimeType)
    {
        return authTimeTypeMapper.selectAuthTimeTypeList(authTimeType);
    }

    /**
     * 查询全部授权时间类型
     * @return
     */
    public List<AuthTimeType> selectAuthTimeTypeAll(){
        return selectAuthTimeTypeList(new AuthTimeType());
    }

    /**
     * 新增授权时间类型
     * 
     * @param authTimeType 授权时间类型
     * @return 结果
     */
    @Override
    public int insertAuthTimeType(AuthTimeType authTimeType)
    {
        return authTimeTypeMapper.insertAuthTimeType(authTimeType);
    }

    /**
     * 修改授权时间类型
     * 
     * @param authTimeType 授权时间类型
     * @return 结果
     */
    @Override
    public int updateAuthTimeType(AuthTimeType authTimeType)
    {
        return authTimeTypeMapper.updateAuthTimeType(authTimeType);
    }

    /**
     * 批量删除授权时间类型
     * 
     * @param timeIds 需要删除的授权时间类型主键
     * @return 结果
     */
    @Override
    public int deleteAuthTimeTypeByTimeIds(Long[] timeIds)
    {
        return authTimeTypeMapper.deleteAuthTimeTypeByTimeIds(timeIds);
    }

    /**
     * 删除授权时间类型信息
     * 
     * @param timeId 授权时间类型主键
     * @return 结果
     */
    @Override
    public int deleteAuthTimeTypeByTimeId(Long timeId)
    {
        return authTimeTypeMapper.deleteAuthTimeTypeByTimeId(timeId);
    }
}
