package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.AuthTimeType;
import com.ruoyi.system.domain.ProductType;

/**
 * 授权时间类型Service接口
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
public interface IAuthTimeTypeService 
{
    /**
     * 查询授权时间类型
     * 
     * @param timeId 授权时间类型主键
     * @return 授权时间类型
     */
    public AuthTimeType selectAuthTimeTypeByTimeId(Long timeId);

    /**
     * 查询授权时间类型列表
     * 
     * @param authTimeType 授权时间类型
     * @return 授权时间类型集合
     */
    public List<AuthTimeType> selectAuthTimeTypeList(AuthTimeType authTimeType);

    /**
     * 查询全部授权时间类型
     * @return
     */
    public List<AuthTimeType> selectAuthTimeTypeAll();


    /**
     * 新增授权时间类型
     * 
     * @param authTimeType 授权时间类型
     * @return 结果
     */
    public int insertAuthTimeType(AuthTimeType authTimeType);

    /**
     * 修改授权时间类型
     * 
     * @param authTimeType 授权时间类型
     * @return 结果
     */
    public int updateAuthTimeType(AuthTimeType authTimeType);

    /**
     * 批量删除授权时间类型
     * 
     * @param timeIds 需要删除的授权时间类型主键集合
     * @return 结果
     */
    public int deleteAuthTimeTypeByTimeIds(Long[] timeIds);

    /**
     * 删除授权时间类型信息
     * 
     * @param timeId 授权时间类型主键
     * @return 结果
     */
    public int deleteAuthTimeTypeByTimeId(Long timeId);
}
