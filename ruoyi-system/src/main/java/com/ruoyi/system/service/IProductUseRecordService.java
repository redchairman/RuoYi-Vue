package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ProductUseRecord;

/**
 * 产品使用记录Service接口
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
public interface IProductUseRecordService 
{
    /**
     * 查询产品使用记录
     * 
     * @param useId 产品使用记录主键
     * @return 产品使用记录
     */
    public ProductUseRecord selectProductUseRecordByUseId(Long useId);

    /**
     * 查询产品使用记录列表
     * 
     * @param productUseRecord 产品使用记录
     * @return 产品使用记录集合
     */
    public List<ProductUseRecord> selectProductUseRecordList(ProductUseRecord productUseRecord);

    /**
     * 新增产品使用记录
     * 
     * @param productUseRecord 产品使用记录
     * @return 结果
     */
    public int insertProductUseRecord(ProductUseRecord productUseRecord);

    /**
     * 修改产品使用记录
     * 
     * @param productUseRecord 产品使用记录
     * @return 结果
     */
    public int updateProductUseRecord(ProductUseRecord productUseRecord);

    /**
     * 批量删除产品使用记录
     * 
     * @param useIds 需要删除的产品使用记录主键集合
     * @return 结果
     */
    public int deleteProductUseRecordByUseIds(Long[] useIds);

    /**
     * 删除产品使用记录信息
     * 
     * @param useId 产品使用记录主键
     * @return 结果
     */
    public int deleteProductUseRecordByUseId(Long useId);
}
