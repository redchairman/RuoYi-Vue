package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ProductOrder;

/**
 * 订单管理Service接口
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
public interface IProductOrderService 
{
    /**
     * 查询订单管理
     * 
     * @param orderId 订单管理主键
     * @return 订单管理
     */
    public ProductOrder selectProductOrderByOrderId(Long orderId);

    /**
     * 查询订单管理列表
     * 
     * @param productOrder 订单管理
     * @return 订单管理集合
     */
    public List<ProductOrder> selectProductOrderList(ProductOrder productOrder);

    /**
     * 新增订单管理
     * 
     * @param productOrder 订单管理
     * @return 结果
     */
    public int insertProductOrder(ProductOrder productOrder);

    /**
     * 修改订单管理
     * 
     * @param productOrder 订单管理
     * @return 结果
     */
    public int updateProductOrder(ProductOrder productOrder);

    /**
     * 批量删除订单管理
     * 
     * @param orderIds 需要删除的订单管理主键集合
     * @return 结果
     */
    public int deleteProductOrderByOrderIds(Long[] orderIds);

    /**
     * 删除订单管理信息
     * 
     * @param orderId 订单管理主键
     * @return 结果
     */
    public int deleteProductOrderByOrderId(Long orderId);
}
