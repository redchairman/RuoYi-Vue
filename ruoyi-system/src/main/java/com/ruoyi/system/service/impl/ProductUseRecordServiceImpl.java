package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ProductUseRecordMapper;
import com.ruoyi.system.domain.ProductUseRecord;
import com.ruoyi.system.service.IProductUseRecordService;

/**
 * 产品使用记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@Service
public class ProductUseRecordServiceImpl implements IProductUseRecordService 
{
    @Autowired
    private ProductUseRecordMapper productUseRecordMapper;

    /**
     * 查询产品使用记录
     * 
     * @param useId 产品使用记录主键
     * @return 产品使用记录
     */
    @Override
    public ProductUseRecord selectProductUseRecordByUseId(Long useId)
    {
        return productUseRecordMapper.selectProductUseRecordByUseId(useId);
    }

    /**
     * 查询产品使用记录列表
     * 
     * @param productUseRecord 产品使用记录
     * @return 产品使用记录
     */
    @Override
    public List<ProductUseRecord> selectProductUseRecordList(ProductUseRecord productUseRecord)
    {
        return productUseRecordMapper.selectProductUseRecordList(productUseRecord);
    }

    /**
     * 新增产品使用记录
     * 
     * @param productUseRecord 产品使用记录
     * @return 结果
     */
    @Override
    public int insertProductUseRecord(ProductUseRecord productUseRecord)
    {
        return productUseRecordMapper.insertProductUseRecord(productUseRecord);
    }

    /**
     * 修改产品使用记录
     * 
     * @param productUseRecord 产品使用记录
     * @return 结果
     */
    @Override
    public int updateProductUseRecord(ProductUseRecord productUseRecord)
    {
        return productUseRecordMapper.updateProductUseRecord(productUseRecord);
    }

    /**
     * 批量删除产品使用记录
     * 
     * @param useIds 需要删除的产品使用记录主键
     * @return 结果
     */
    @Override
    public int deleteProductUseRecordByUseIds(Long[] useIds)
    {
        return productUseRecordMapper.deleteProductUseRecordByUseIds(useIds);
    }

    /**
     * 删除产品使用记录信息
     * 
     * @param useId 产品使用记录主键
     * @return 结果
     */
    @Override
    public int deleteProductUseRecordByUseId(Long useId)
    {
        return productUseRecordMapper.deleteProductUseRecordByUseId(useId);
    }
}
