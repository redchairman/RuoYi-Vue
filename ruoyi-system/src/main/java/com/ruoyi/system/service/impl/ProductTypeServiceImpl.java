package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ProductTypeMapper;
import com.ruoyi.system.domain.ProductType;
import com.ruoyi.system.service.IProductTypeService;

/**
 * 产品Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@Service
public class ProductTypeServiceImpl implements IProductTypeService 
{
    @Autowired
    private ProductTypeMapper productTypeMapper;

    /**
     * 查询产品
     * 
     * @param productId 产品主键
     * @return 产品
     */
    @Override
    public ProductType selectProductTypeByProductId(Long productId)
    {
        return productTypeMapper.selectProductTypeByProductId(productId);
    }

    /**
     * 查询产品
     *
     * @param productCode 产品Code
     * @return 产品
     */
    public ProductType selectProductTypeByProductCode(String productCode)
    {
        return productTypeMapper.selectProductTypeByProductCode(productCode);
    }


    /**
     * 查询产品列表
     * 
     * @param productType 产品
     * @return 产品
     */
    @Override
    public List<ProductType> selectProductTypeList(ProductType productType)
    {
        return productTypeMapper.selectProductTypeList(productType);
    }

    /**
     * 查询全部产品列表
     * @return
     */
    @Override
    public List<ProductType> selectProductTypeAll()
    {
        return selectProductTypeList(new ProductType());
    }


    /**
     * 新增产品
     * 
     * @param productType 产品
     * @return 结果
     */
    @Override
    public int insertProductType(ProductType productType)
    {
        return productTypeMapper.insertProductType(productType);
    }

    /**
     * 修改产品
     * 
     * @param productType 产品
     * @return 结果
     */
    @Override
    public int updateProductType(ProductType productType)
    {
        return productTypeMapper.updateProductType(productType);
    }

    /**
     * 批量删除产品
     * 
     * @param productIds 需要删除的产品主键
     * @return 结果
     */
    @Override
    public int deleteProductTypeByProductIds(Long[] productIds)
    {
        return productTypeMapper.deleteProductTypeByProductIds(productIds);
    }

    /**
     * 删除产品信息
     * 
     * @param productId 产品主键
     * @return 结果
     */
    @Override
    public int deleteProductTypeByProductId(Long productId)
    {
        return productTypeMapper.deleteProductTypeByProductId(productId);
    }
}
