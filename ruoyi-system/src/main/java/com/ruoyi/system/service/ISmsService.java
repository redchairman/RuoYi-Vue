package com.ruoyi.system.service;

public interface ISmsService {

    /**
     * 发送短信的验证码
     *
     * @param phone
     * @return
     */
    String sendSms(String phone, String code);

    /**
     * 验证验证码
     *
     * @param phone
     * @param code
     * @return
     */
    Boolean validationCode(String phone, String code);

}
