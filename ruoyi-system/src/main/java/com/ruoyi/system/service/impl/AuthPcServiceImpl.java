package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.AuthPcMapper;
import com.ruoyi.system.domain.AuthPc;
import com.ruoyi.system.service.IAuthPcService;

/**
 * 授权计算机Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@Service
public class AuthPcServiceImpl implements IAuthPcService 
{
    @Autowired
    private AuthPcMapper authPcMapper;

    /**
     * 查询授权计算机
     * 
     * @param pcId 授权计算机主键
     * @return 授权计算机
     */
    @Override
    public AuthPc selectAuthPcByPcId(Long pcId)
    {
        return authPcMapper.selectAuthPcByPcId(pcId);
    }

    /**
     * 查询授权计算机列表
     * 
     * @param authPc 授权计算机
     * @return 授权计算机
     */
    @Override
    public List<AuthPc> selectAuthPcList(AuthPc authPc)
    {
        return authPcMapper.selectAuthPcList(authPc);
    }

    /**
     * 新增授权计算机
     * 
     * @param authPc 授权计算机
     * @return 结果
     */
    @Override
    public int insertAuthPc(AuthPc authPc)
    {
        authPc.setCreateTime(DateUtils.getNowDate());
        return authPcMapper.insertAuthPc(authPc);
    }

    /**
     * 修改授权计算机
     * 
     * @param authPc 授权计算机
     * @return 结果
     */
    @Override
    public int updateAuthPc(AuthPc authPc)
    {
        return authPcMapper.updateAuthPc(authPc);
    }

    /**
     * 批量删除授权计算机
     * 
     * @param pcIds 需要删除的授权计算机主键
     * @return 结果
     */
    @Override
    public int deleteAuthPcByPcIds(Long[] pcIds)
    {
        return authPcMapper.deleteAuthPcByPcIds(pcIds);
    }

    /**
     * 删除授权计算机信息
     * 
     * @param pcId 授权计算机主键
     * @return 结果
     */
    @Override
    public int deleteAuthPcByPcId(Long pcId)
    {
        return authPcMapper.deleteAuthPcByPcId(pcId);
    }
}
