package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ProductPriceMapper;
import com.ruoyi.system.domain.ProductPrice;
import com.ruoyi.system.service.IProductPriceService;

/**
 * 价格Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@Service
public class ProductPriceServiceImpl implements IProductPriceService 
{
    @Autowired
    private ProductPriceMapper productPriceMapper;

    /**
     * 查询价格
     * 
     * @param priceId 价格主键
     * @return 价格
     */
    @Override
    public ProductPrice selectProductPriceByPriceId(Long priceId)
    {
        return productPriceMapper.selectProductPriceByPriceId(priceId);
    }

    /**
     * 查询价格列表
     * 
     * @param productPrice 价格
     * @return 价格
     */
    @Override
    public List<ProductPrice> selectProductPriceList(ProductPrice productPrice)
    {
        return productPriceMapper.selectProductPriceList(productPrice);
    }

    /**
     * 新增价格
     * 
     * @param productPrice 价格
     * @return 结果
     */
    @Override
    public int insertProductPrice(ProductPrice productPrice)
    {
        return productPriceMapper.insertProductPrice(productPrice);
    }

    /**
     * 修改价格
     * 
     * @param productPrice 价格
     * @return 结果
     */
    @Override
    public int updateProductPrice(ProductPrice productPrice)
    {
        return productPriceMapper.updateProductPrice(productPrice);
    }

    /**
     * 批量删除价格
     * 
     * @param priceIds 需要删除的价格主键
     * @return 结果
     */
    @Override
    public int deleteProductPriceByPriceIds(Long[] priceIds)
    {
        return productPriceMapper.deleteProductPriceByPriceIds(priceIds);
    }

    /**
     * 删除价格信息
     * 
     * @param priceId 价格主键
     * @return 结果
     */
    @Override
    public int deleteProductPriceByPriceId(Long priceId)
    {
        return productPriceMapper.deleteProductPriceByPriceId(priceId);
    }
}
