package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.AuthPc;

/**
 * 授权计算机Service接口
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
public interface IAuthPcService 
{
    /**
     * 查询授权计算机
     * 
     * @param pcId 授权计算机主键
     * @return 授权计算机
     */
    public AuthPc selectAuthPcByPcId(Long pcId);

    /**
     * 查询授权计算机列表
     * 
     * @param authPc 授权计算机
     * @return 授权计算机集合
     */
    public List<AuthPc> selectAuthPcList(AuthPc authPc);

    /**
     * 新增授权计算机
     * 
     * @param authPc 授权计算机
     * @return 结果
     */
    public int insertAuthPc(AuthPc authPc);

    /**
     * 修改授权计算机
     * 
     * @param authPc 授权计算机
     * @return 结果
     */
    public int updateAuthPc(AuthPc authPc);

    /**
     * 批量删除授权计算机
     * 
     * @param pcIds 需要删除的授权计算机主键集合
     * @return 结果
     */
    public int deleteAuthPcByPcIds(Long[] pcIds);

    /**
     * 删除授权计算机信息
     * 
     * @param pcId 授权计算机主键
     * @return 结果
     */
    public int deleteAuthPcByPcId(Long pcId);
}
