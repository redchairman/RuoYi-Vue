package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ProductType;

/**
 * 产品Service接口
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
public interface IProductTypeService 
{
    /**
     * 查询产品
     * 
     * @param productId 产品主键
     * @return 产品
     */
    public ProductType selectProductTypeByProductId(Long productId);

    /**
     * 查询产品
     *
     * @param productCode 产品Code
     * @return 产品
     */
    public ProductType selectProductTypeByProductCode(String productCode);

    /**
     * 查询产品列表
     * 
     * @param productType 产品
     * @return 产品集合
     */
    public List<ProductType> selectProductTypeList(ProductType productType);

    /**
     * 查询全部产品列表
     * @return
     */
    public List<ProductType> selectProductTypeAll();

    /**
     * 新增产品
     * 
     * @param productType 产品
     * @return 结果
     */
    public int insertProductType(ProductType productType);

    /**
     * 修改产品
     * 
     * @param productType 产品
     * @return 结果
     */
    public int updateProductType(ProductType productType);

    /**
     * 批量删除产品
     * 
     * @param productIds 需要删除的产品主键集合
     * @return 结果
     */
    public int deleteProductTypeByProductIds(Long[] productIds);

    /**
     * 删除产品信息
     * 
     * @param productId 产品主键
     * @return 结果
     */
    public int deleteProductTypeByProductId(Long productId);
}
