package com.ruoyi.system.service;

import com.ruoyi.system.domain.ProductOrder;

public interface IWxPayV3Service {

    public String nativePay(ProductOrder order);
}
