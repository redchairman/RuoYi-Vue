package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 授权计算机对象 auth_pc
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
public class AuthPc extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 电脑ID */
    private Long pcId;

    /** pc序列号 */
    @Excel(name = "pc序列号")
    private String pcCode;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 删除标记 */
    private String delFlag;

    public void setPcId(Long pcId) 
    {
        this.pcId = pcId;
    }

    public Long getPcId() 
    {
        return pcId;
    }
    public void setPcCode(String pcCode) 
    {
        this.pcCode = pcCode;
    }

    public String getPcCode() 
    {
        return pcCode;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("pcId", getPcId())
            .append("pcCode", getPcCode())
            .append("userId", getUserId())
            .append("delFlag", getDelFlag())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .toString();
    }
}
