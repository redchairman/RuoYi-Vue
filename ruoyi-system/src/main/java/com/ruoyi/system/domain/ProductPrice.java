package com.ruoyi.system.domain;

import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.common.annotation.Excels;
import com.ruoyi.common.core.domain.entity.SysDept;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 价格对象 product_price
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
public class ProductPrice extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 价格ID */
    private Long priceId;

    /** 产品ID */
    @Excel(name = "产品ID")
    private Long productId;

    /** 部门对象 */
    @Excels({
            @Excel(name = "产品名称", targetAttr = "productName", type = Excel.Type.EXPORT)
    })
    private ProductType productType;


    /** 授权时间 */
    @Excel(name = "授权时间")
    private Long authTime;

    /** 授权时间单位 */
    @Excel(name = "授权时间单位")
    private String authTimeUnit;

    /** 电脑数 */
    @Excel(name = "电脑数")
    private Long pcCount;

    /** 使用次数 */
    @Excel(name = "使用次数")
    private Long useCount;

    /** 价格 */
    @Excel(name = "价格")
    private Long price;

    @Excel(name = "时长描述")
    private String timeDesc;

    public void setPriceId(Long priceId) 
    {
        this.priceId = priceId;
    }

    public Long getPriceId() 
    {
        return priceId;
    }
    public void setProductId(Long productId) 
    {
        this.productId = productId;
    }

    public Long getProductId() 
    {
        return productId;
    }

    public void setPcCount(Long pcCount) 
    {
        this.pcCount = pcCount;
    }

    public Long getPcCount() 
    {
        return pcCount;
    }
    public void setUseCount(Long useCount) 
    {
        this.useCount = useCount;
    }

    public Long getUseCount() 
    {
        return useCount;
    }
    public void setPrice(Long price)
    {
        this.price = price;
    }

    public Long getPrice()
    {
        return price;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("priceId", getPriceId())
            .append("productId", getProductId())
            .append("authTime", getAuthTime())
            .append("authTimeUnit", getAuthTimeUnit())
            .append("pcCount", getPcCount())
            .append("useCount", getUseCount())
            .append("price", getPrice())
            .append("desc", getTimeDesc())
            .toString();
    }
    public JSONObject toJsonObject()
    {
        JSONObject obj = new JSONObject();
        obj.put("priceId", getPriceId());
        obj.put("productId", getProductId());
        obj.put("productId", getProductId());
        obj.put("authTime", getAuthTime());
        obj.put("authTimeUnit", getAuthTimeUnit());
        obj.put("pcCount", getPcCount());
        obj.put("useCount", getUseCount());
        obj.put("price", getPrice());
        obj.put("desc", getTimeDesc());
        return obj;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public String getTimeDesc() {
        return timeDesc;
    }

    public void setTimeDesc(String timeDesc) {
        this.timeDesc = timeDesc;
    }

    public Long getAuthTime() {
        return authTime;
    }

    public void setAuthTime(Long authTime) {
        this.authTime = authTime;
    }

    public String getAuthTimeUnit() {
        return authTimeUnit;
    }

    public void setAuthTimeUnit(String authTimeUnit) {
        this.authTimeUnit = authTimeUnit;
    }
}
