package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 产品使用记录对象 product_use_record
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
public class ProductUseRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    private Long useId;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 训练类型 */
    @Excel(name = "训练类型")
    private Long trainType;

    /** 训练精度 */
    @Excel(name = "训练精度")
    private Long trainPrecision;

    /** 训练点数 */
    @Excel(name = "训练点数")
    private Long pointCount;

    /** MAC地址 */
    @Excel(name = "MAC地址")
    private String pcMac;

    /** os */
    @Excel(name = "os")
    private String pcOs;

    /** 机器码 */
    @Excel(name = "机器码")
    private String pcCode;

    public void setUseId(Long useId) 
    {
        this.useId = useId;
    }

    public Long getUseId() 
    {
        return useId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setStartTime(Date startTime) 
    {
        this.startTime = startTime;
    }

    public Date getStartTime() 
    {
        return startTime;
    }
    public void setEndTime(Date endTime) 
    {
        this.endTime = endTime;
    }

    public Date getEndTime() 
    {
        return endTime;
    }
    public void setTrainType(Long trainType) 
    {
        this.trainType = trainType;
    }

    public Long getTrainType() 
    {
        return trainType;
    }
    public void setTrainPrecision(Long trainPrecision) 
    {
        this.trainPrecision = trainPrecision;
    }

    public Long getTrainPrecision() 
    {
        return trainPrecision;
    }
    public void setPointCount(Long pointCount) 
    {
        this.pointCount = pointCount;
    }

    public Long getPointCount() 
    {
        return pointCount;
    }
    public void setPcMac(String pcMac) 
    {
        this.pcMac = pcMac;
    }

    public String getPcMac() 
    {
        return pcMac;
    }
    public void setPcOs(String pcOs) 
    {
        this.pcOs = pcOs;
    }

    public String getPcOs() 
    {
        return pcOs;
    }
    public void setPcCode(String pcCode) 
    {
        this.pcCode = pcCode;
    }

    public String getPcCode() 
    {
        return pcCode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("useId", getUseId())
            .append("userId", getUserId())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("trainType", getTrainType())
            .append("trainPrecision", getTrainPrecision())
            .append("pointCount", getPointCount())
            .append("pcMac", getPcMac())
            .append("pcOs", getPcOs())
            .append("pcCode", getPcCode())
            .toString();
    }
}
