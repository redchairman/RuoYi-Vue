package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 订单管理对象 product_order
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
public class ProductOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    private Long orderId;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private String orderNumber;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;

    /** 订单状态 */
    @Excel(name = "订单状态")
    private int state;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 到期时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "到期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date expireTime;

    /** 可用天数 */
    @Excel(name = "可用天数")
    private Long availDay;

    /** 可用次数 */
    @Excel(name = "可用次数")
    private Long availCount;

    /** 下单时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "下单时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date orderTime;

    /** 付款时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "付款时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date payTime;

    /** 金额 */
    @Excel(name = "金额")
    private Long price;

    /** 付款方式 */
    @Excel(name = "付款方式")
    private Long payMethod;

    /** 付款ID */
    @Excel(name = "付款ID")
    private String payId;

    /** 付款二维码 */
    private String qrCodeUrl;

    /** 产品标识 */
    @Excel(name = "产品标识")
    private String productCode;

    /** 价格套餐id */
    @Excel(name = "价格套餐id")
    private Long priceId;


    public void setOrderId(Long orderId) 
    {
        this.orderId = orderId;
    }

    public Long getOrderId() 
    {
        return orderId;
    }
    public void setOrderNumber(String orderNumber) 
    {
        this.orderNumber = orderNumber;
    }

    public String getOrderNumber() 
    {
        return orderNumber;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setState(int state)
    {
        this.state = state;
    }

    public int getState()
    {
        return state;
    }
    public void setStartTime(Date startTime) 
    {
        this.startTime = startTime;
    }

    public Date getStartTime() 
    {
        return startTime;
    }
    public void setExpireTime(Date expireTime) 
    {
        this.expireTime = expireTime;
    }

    public Date getExpireTime() 
    {
        return expireTime;
    }
    public void setAvailDay(Long availDay) 
    {
        this.availDay = availDay;
    }

    public Long getAvailDay() 
    {
        return availDay;
    }
    public void setAvailCount(Long availCount) 
    {
        this.availCount = availCount;
    }

    public Long getAvailCount() 
    {
        return availCount;
    }
    public void setOrderTime(Date orderTime) 
    {
        this.orderTime = orderTime;
    }

    public Date getOrderTime() 
    {
        return orderTime;
    }
    public void setPayTime(Date payTime) 
    {
        this.payTime = payTime;
    }

    public Date getPayTime() 
    {
        return payTime;
    }
    public void setPrice(Long price) 
    {
        this.price = price;
    }

    public Long getPrice() 
    {
        return price;
    }
    public void setPayMethod(Long payMethod) 
    {
        this.payMethod = payMethod;
    }

    public Long getPayMethod() 
    {
        return payMethod;
    }
    public void setPayId(String payId) 
    {
        this.payId = payId;
    }

    public String getPayId() 
    {
        return payId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("orderId", getOrderId())
            .append("orderNumber", getOrderNumber())
            .append("userId", getUserId())
            .append("state", getState())
            .append("startTime", getStartTime())
            .append("expireTime", getExpireTime())
            .append("availDay", getAvailDay())
            .append("availCount", getAvailCount())
            .append("orderTime", getOrderTime())
            .append("payTime", getPayTime())
            .append("price", getPrice())
            .append("payMethod", getPayMethod())
            .append("payId", getPayId())
            .toString();
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Long getPriceId() {
        return priceId;
    }

    public void setPriceId(Long priceId) {
        this.priceId = priceId;
    }

    public String getQrCodeUrl() {
        return qrCodeUrl;
    }

    public void setQrCodeUrl(String qrCodeUrl) {
        this.qrCodeUrl = qrCodeUrl;
    }
}
