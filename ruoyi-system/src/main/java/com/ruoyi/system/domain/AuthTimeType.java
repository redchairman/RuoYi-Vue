package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 授权时间类型对象 auth_time_type
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
public class AuthTimeType extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long timeId;

    /** 天数，-1表示永久授权 */
    @Excel(name = "天数，-1表示永久授权")
    private Long days;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    public void setTimeId(Long timeId) 
    {
        this.timeId = timeId;
    }

    public Long getTimeId() 
    {
        return timeId;
    }
    public void setDays(Long days) 
    {
        this.days = days;
    }

    public Long getDays() 
    {
        return days;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("timeId", getTimeId())
            .append("days", getDays())
            .append("name", getName())
            .toString();
    }
}
