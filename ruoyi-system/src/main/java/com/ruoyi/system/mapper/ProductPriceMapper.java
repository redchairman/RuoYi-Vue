package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.ProductPrice;

/**
 * 价格Mapper接口
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
public interface ProductPriceMapper 
{
    /**
     * 查询价格
     * 
     * @param priceId 价格主键
     * @return 价格
     */
    public ProductPrice selectProductPriceByPriceId(Long priceId);

    /**
     * 查询价格列表
     * 
     * @param productPrice 价格
     * @return 价格集合
     */
    public List<ProductPrice> selectProductPriceList(ProductPrice productPrice);

    /**
     * 新增价格
     * 
     * @param productPrice 价格
     * @return 结果
     */
    public int insertProductPrice(ProductPrice productPrice);

    /**
     * 修改价格
     * 
     * @param productPrice 价格
     * @return 结果
     */
    public int updateProductPrice(ProductPrice productPrice);

    /**
     * 删除价格
     * 
     * @param priceId 价格主键
     * @return 结果
     */
    public int deleteProductPriceByPriceId(Long priceId);

    /**
     * 批量删除价格
     * 
     * @param priceIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProductPriceByPriceIds(Long[] priceIds);
}
