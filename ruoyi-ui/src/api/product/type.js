import request from '@/utils/request'

// 查询产品列表
export function listType(query) {
  return request({
    url: '/product/type/list',
    method: 'get',
    params: query
  })
}

// 查询产品详细
export function getType(productId) {
  return request({
    url: '/product/type/' + productId,
    method: 'get'
  })
}

// 新增产品
export function addType(data) {
  return request({
    url: '/product/type',
    method: 'post',
    data: data
  })
}

// 修改产品
export function updateType(data) {
  return request({
    url: '/product/type',
    method: 'put',
    data: data
  })
}

// 删除产品
export function delType(productId) {
  return request({
    url: '/product/type/' + productId,
    method: 'delete'
  })
}
