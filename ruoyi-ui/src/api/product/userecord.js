import request from '@/utils/request'

// 查询产品使用记录列表
export function listUserecord(query) {
  return request({
    url: '/product/userecord/list',
    method: 'get',
    params: query
  })
}

// 查询产品使用记录详细
export function getUserecord(useId) {
  return request({
    url: '/product/userecord/' + useId,
    method: 'get'
  })
}

// 新增产品使用记录
export function addUserecord(data) {
  return request({
    url: '/product/userecord',
    method: 'post',
    data: data
  })
}

// 修改产品使用记录
export function updateUserecord(data) {
  return request({
    url: '/product/userecord',
    method: 'put',
    data: data
  })
}

// 删除产品使用记录
export function delUserecord(useId) {
  return request({
    url: '/product/userecord/' + useId,
    method: 'delete'
  })
}
