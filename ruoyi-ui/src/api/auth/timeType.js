import request from '@/utils/request'

// 查询授权时间类型列表
export function listTimeType(query) {
  return request({
    url: '/auth/timeType/list',
    method: 'get',
    params: query
  })
}

// 查询授权时间类型详细
export function getTimeType(timeId) {
  return request({
    url: '/auth/timeType/' + timeId,
    method: 'get'
  })
}

// 新增授权时间类型
export function addTimeType(data) {
  return request({
    url: '/auth/timeType',
    method: 'post',
    data: data
  })
}

// 修改授权时间类型
export function updateTimeType(data) {
  return request({
    url: '/auth/timeType',
    method: 'put',
    data: data
  })
}

// 删除授权时间类型
export function delTimeType(timeId) {
  return request({
    url: '/auth/timeType/' + timeId,
    method: 'delete'
  })
}
