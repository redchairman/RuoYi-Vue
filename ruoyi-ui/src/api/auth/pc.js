import request from '@/utils/request'

// 查询授权计算机列表
export function listPc(query) {
  return request({
    url: '/auth/pc/list',
    method: 'get',
    params: query
  })
}

// 查询授权计算机详细
export function getPc(pcId) {
  return request({
    url: '/auth/pc/' + pcId,
    method: 'get'
  })
}

// 新增授权计算机
export function addPc(data) {
  return request({
    url: '/auth/pc',
    method: 'post',
    data: data
  })
}

// 修改授权计算机
export function updatePc(data) {
  return request({
    url: '/auth/pc',
    method: 'put',
    data: data
  })
}

// 删除授权计算机
export function delPc(pcId) {
  return request({
    url: '/auth/pc/' + pcId,
    method: 'delete'
  })
}
