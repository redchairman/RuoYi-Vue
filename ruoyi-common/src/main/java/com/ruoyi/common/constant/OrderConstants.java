package com.ruoyi.common.constant;

public class OrderConstants {

    public static final int ORDER_STATUS_INIT = 0;
    public static final int ORDER_STATUS_WAIT_PAY = 1;
    public static final int ORDER_STATUS_FINISH = 2;
    public static final int ORDER_STATUS_FAIL = 3;
    public static final int ORDER_STATUS_CANCEL = 4;


}
